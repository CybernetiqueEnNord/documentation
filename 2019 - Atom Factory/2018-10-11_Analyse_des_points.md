
# Analyse des points de la table (FINAL)

### Points pr�sents sur la table :

On suppose ici que tout le monde marque un maximum de points avec les �l�ments de jeu disponibles.

Chaque �quipe dispose des �l�ments suivants :
 - 1 goldenium
 - 4 blueium
 - 6 greenium
 - 8 redium

Dans ce cas, le d�compte des points est le suivant :
 - 10 points pour avoir ouvert l'acc�l�rateur
 - 20 points pour avoir d�croch� le goldenium
 - 24 points pour avoir le goldenium dans la balance
 - 48 points pour avoir les 4 blueium dans la balance (4 x 12)
 -  8 points pour avoir un greenium dans la balance
 - 80 points pour avoir les 8 redium dans l'acc�l�rateur
 - 10 points pour avoir un greenium dans l'acc�l�rateur
 - 24 points pour mettre les 4 greenium restants dans la zone greenium (4 x (1 + 5))
 -  5 points pour avoir fait l'exp�rience
 - 15 points pour avoir activ� l'exp�rience
 - 20 points pour que l'�lectron rejoigne l'oxyg�ne
 - 88 points bonus pour avoir estim� son score correctement (264 points)
 - 10 points bonus de participation

Total des points sur la table : 724 points.
Total des points par �quipe : 362 points.


### Points atteignables par une seule �quipe (par exemple si l'autre �quipe est forfait) :

le goldenium, 2 blueium, 1 greenium et 1 redium sont r�serv�s � notre adversaire, on dispose donc de :
 - 1 goldenium
 - 6 blueium
 - 11 greenium
 - 15 redium

 - 10 points pour avoir ouvert l'acc�l�rateur
 - 20 points pour avoir d�croch� le goldenium
 - 24 points pour le goldenium dans la balance
 - 60 points pour avoir 5 blueium dans la balance
 - 90 points pour avoir 9 redium dans l'acc�l�rateur
 -  6 points pour avoir le blueium restant dans la zone blueium
 - 66 points pour avoir les 11 greenium dans la zone greenium (11 x (1 + 5))
 - 36 points pour avoir les 6 redium restants dans la zone redium (6 x (1 + 5))
 -  5 points pour avoir fait l'exp�rience
 - 15 points pour avoir activ� d'exp�rience
 - 20 points pour que l'�lectron rejoigne l'oxyg�ne
 - 117 points bonus pour avoir estim� son score correctement (352 points)
 - 10 points bonus de participation

Total pour une seule �quipe : 479 points.


### Points atteignables en match miroir : 

Il n'y a pas d'�l�ments � prendre chez l'adversaire donc le total est celui de la moiti� du terrain.
 - Total pour chaque �quipe : 362 points.


### Estimation du nombre de points n�cessaires pour scorer :

Au vu de l'exp�rience des ann�es pass�es, il est suffisant de faire 40% des points par match pour finir premier, soit 144 points par match (720 points � la fin des qualifications).

De la m�me mani�re, il est suffisant de faire 20% des points par match pour �tre dans le top16, soit 72 points par match (360 points � la fin des qualifications).

NB : au vu du fait qu'un seul �l�ment est manipul� cette ann�e (les atomes), il est � parier que le vrai montant soit plus �lev�.
