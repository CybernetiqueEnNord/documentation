
# Analyse des points de la table

### Points présents sur la table :

On suppose ici que tout le monde marque un maximum de points avec les éléments de jeu disponibles.

- 40 points pour vider les récupérateurs d'eau
- 320 points pour envoyer toutes les balles dans les bonnes stations d'épuration
- 0 points pour envoyer les balles dans les châteaux d'eau

- 34 cubes que l'on peut découper en 6 colonnes de 5 cubes, + 2 colonnes de 2 cubes "bonus"
	chaque colonne comptant pour (1+2+3+4+5+30) 45 points,
	soit un total de (45*6 + 2(1+2)) 276 points

- 10 points pour construire le panneau domotique
- 50 points pour alimenter le panneau domotique

- 10 points pour construire l'abeille
- 100 points pour butiner la fleur

- 103 points pour estimer son score correctement si on fait plus de 501 points

- 20 points de participation

Total sur la table : 929 points.
Total pour une équipe : 465 points.


### Points atteignables par une seule équipe (par exemple si l'autre équipe est forfait) :

- 20 points pour vider les récupérateurs d'eau
- 60 points pour envoyer toutes les balles de sa couleur dans son château d'eau
- 40 points pour envoyer les balles de la couleur adverse dans la station d'épuration

- 32 cubes que l'on peut découper en 6 colonnes de 5 cubes + 1 colonne de 2 cubes "bonus"
	soit un total de 273 points

- 5 points pour construire le panneau domotique
- 25 points pour alimenter le panneau domotique

- 5 points pour construire l'abeille
- 50 points pour butiner la fleur

- 97 points pour estimer son score correctement si on fait 500 points

- 10 points de participation

Total pour une seule équipe : 585 points


### Points atteignables en match miroir (l'adversaire met autant de points que vous) :

- 20 points pour vider les récupérateurs d'eau
- 80 points pour envoyer toutes les balles de sa couleur dans le château d'eau, y compris celles mises dans la station d'épuration par l'adversaire
- 0 points pour envoyer les balles de sa couleur dans la station d'épuration adverse

- 17 cubes que l'on peut découper en 3 colonnes de 5 cubes + 1 colonne de 2 cubes "bonus"
	soit un total de 138 points

- 5 points pour construire le panneau domotique
- 25 points pour alimenter le panneau domotique

- 5 points pour construire l'abeille
- 50 points pour butiner la fleur

- 77 points pour estimer son score correctement si on fait 323

- 10 points de participation

Total pour chaque équipe : 410 points


### Estimation du nombre de points nécessaires pour scorer :

Au vu de l'expérience précédente, il est suffisant de faire 40% des points par match pour finir premier, soit 164 points par match (820 points à la fin des qualifications).

De la même manière, il est suffisant de faire 20% des points par match pour être dans le top8, soit 82 points par match (410 points à la fin des qualifications).

NB : au vu des nombres élevés de points attribués à l'abeille et au panneau domotique (85 points) il est à parier que le vrai montant soit légèrement plus élevé.
