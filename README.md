# documentation
All the docs for the Cup of Robotics Eurobot

Eurobot :
- http://www.eurobot.org

Coupe de France de robotique:
- http://www.coupederobotique.fr/

Coupe de Belgique de robotique:
- http://robots.pass.be/

Coupe de Suisse de robotique:
- http://www.robot-ch.org/
- https://www.swisseurobot.ch/fr/

