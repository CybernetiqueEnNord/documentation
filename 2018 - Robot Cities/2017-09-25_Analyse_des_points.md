
# Analyse des points de la table

### Points pr�sents sur la table :

On suppose ici que tout le monde marque un maximum de points avec les �l�ments de jeu disponibles.

- 40 points pour vider les r�cup�rateurs d'eau
- 320 points pour envoyer toutes les balles dans les bons chateaux d'eau
- 0 points pour envoyer les balles dans les stations de r�cup�ration

- 34 cubes que l'on peut d�couper en 6 colonnes de 5 cubes + 2 colonnes de 2 cubes "bonus"
	chaque colonne comptant pour (2+3+4+5+6+15) 35 points,
	soit un total de (35*6 + 2*(2+3)) 220 points

- 10 points pour construire un panneau domotique
- 50 points pour alimenter le panneau domotique

- 10 points pour construire l'abeille
- 100 points pour butiner la fleur

- 103 points pour estimer son score correctement si on fait plus de 501 points

- 20 points de participation

Total sur la table : 873 points.
Total pour une �quipe : 437 points.


### Points atteignables par une seule �quipe (par exemple si l'autre �quipe est forfait) :

- 20 points pour vider les r�cup�rateurs d'eau
- 160 points pour envoyer toutes les balles de sa couleur dans le chateau d'eau, y compris celles mises dans la station de recyclage par l'adversaire
- 20 points pour envoyer les balles de sa couleur dans sa station de recyclage

- 32 cubes que l'on peut d�couper en 6 colonnes de 5 cubes + 1 colonne de 2 cubes "bonus"
	soit un total de 215 points

- 5 points pour construire le panneau domotique
- 25 points pour alimenter le panneau domotique

- 5 points pour construire l'abeille
- 50 points pour butiner la fleur

- 97 points pour estimer son score correctement si on fait 500 points

- 10 points de participation

Total pour une seule �quipe : 607 points.


### Points atteignables en match miroir (l'adversaire met autant de points que vous) :

- 20 points pour vider les r�cup�rateurs d'eau
- 160 points pour envoyer toutes les balles de sa couleur dans le chateau d'eau, y compris celles mises dans la station de recyclage par l'adversaire
- 0 points pour envoyer les balles de sa couleur dans les stations de r�cup�ration

- 17 cubes que l'on peut d�couper en 3 colonnes de 5 cubes + 1 colonne de 2 cubes "bonus"
	soit un total de 110 points

- 5 points pour construire le panneau domotique
- 25 points pour alimenter le panneau domotique

- 5 points pour construire l'abeille
- 50 points pour butiner la fleur

- 83 points pour estimer son score correctement si on fait 375 points

- 10 points de participation

Total pour chaque �quipe : 468 points.


### Estimation du nombre de points n�cessaires pour scorer :

Au vu de l'exp�rience pr�c�dente, il est suffisant de faire 40% des points par match pour finir premier, soit 187 points par match (936 points � la fin des qualifications).

De la m�me mani�re, il est suffisant de faire 20% des points par match pour �tre dans le top8, soit 94 points par match (468 points � la fin des qualifications).
