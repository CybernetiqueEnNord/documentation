
# Analyse des points de la table (BETA)

### Points pr�sents sur la table :

On suppose ici que tout le monde marque un maximum de points avec les �l�ments de jeu disponibles.

Chaque �quipe dispose des atomes suivants :
 - 1 goldenium
 - 3 blueium
 - 5 greenium (1 r�serv�)
 - 10 redium (2 r�serv�s)

Dans ce cas, le d�compte des points est le suivant : 
 - 10 points pour avoir ouvert l'acc�l�rateur
 - 20 points pour avoir d�croch� le goldenium
 - 24 points pour le goldenium dans la balance
 - 36 points pour les 3 blueium dans la balance (3 x 12)
 - 16 points pour 2 greenium dans la balance (2 x 8)
 - 90 points pour les 9 redium dans l'acc�l�rateur (9 x 10)
 - 18 points pour les 3 greenium restants dans la zone greenium (3 x ( 1 + 5))
 - 6 points pour le redium restant dans la zone redium (1 + 5)
 - 5 points pour avoir fait l'exp�rience
 - 15 points pour avoir activ� l'exp�rience
 - 20 points pour que l'�lectron rejoigne l'oxyg�ne
 - 78 points bonus pour avoir estim� son score correctement (260 points)
 - 10 points bonus de participation

Total de points sur la table : 696 points.
Total de points par �quipe : 348 points.


### Points atteignables par une seule �quipe (par exemple si l'autre �quipe est forfait) :

 - 10 points pour avoir ouvert l'acc�l�rateur
 - 20 points pour avoir d�croch� le goldenium
 - 24 points pour le goldenium dans la balance
 - 60 points pour les 5 blueium dans la balance (5 x 12)
 - 90 points pour les 9 redium dans l'acc�l�rateur (9 x 10)
 - 6 points pour le blueium restant dans la zone blueium (1 + 5)
 - 54 points pour les 9 greenium dans la zone greenium (9 x (1 + 5))
 - 54 points pour les 9 redium dans la zone redium (9 x (1 + 5))
 - 5 points pour avoir fait l'exp�rience
 - 15 points pour avoir activ� l'exp�rience
 - 20 points pour que l'�lectron rejoigne l'oxyg�ne
 - 107 points pour avoir estim� son score correctement (358 points)
 - 10 points bonus de participation 

Total pour une seule �quipe : 475 points.


### Points atteignables en match miroir :

Il n'y a pas d'�l�ments � prendre chez l'adversaire, donc le total est celui de la moiti� du terrain.
 - Total pour chaque �quipe : 348 points.


### Estimation du nombre de points n�cessaires pour scorer :

Au vu de l'exp�rience des ann�es pass�es, il est suffisant de faire 40% des points par match pour finir premier, soit 139 points par match (695 points � la fin des qualifications).

De la m�me mani�re, il est suffisant de faire 20% des points par match pour �tre dans le top16, soit 70 points par match (350 points � la fin des qualifications).

NB : au vu du fait qu'un seul �l�ment est manipul� cette ann�e (les atomes), il est � parier que le vrai montant soit plus �lev�.
