# Donn�es de la coupe de France de Robotique 2017

Les fichiers de donn�es csv sont encod�s en UTF-8

Sources:

- pour la liste des �quipes inscrites 
  - http://www.coupederobotique.fr/coupe-2017/le-concours/equipes/
- pour le classement � la fin du 1er tour 
  - http://www.coupederobotique.fr/cdr2017-classement-serie-n1/
- pour le classement � la fin du 2e tour
  - http://www.coupederobotique.fr/cdr2017-classement-serie-n2/
- pour le classement � la fin du 3e tour
  - http://www.coupederobotique.fr/cdr2017-classement-serie-n3/
- pour le classement � la fin du 5e tour
  - http://www.coupederobotique.fr/cdr-2017-classement-serie-n5/

- pour le classement � la fin du 1er tour EUROBOT
  - http://www.coupederobotique.fr/eurobot-2017-ranking-round-n1/
- pour le classement � la fin du 2e tour EUROBOT
  - http://www.coupederobotique.fr/eurobot-2017-ranking-round-n2/
- pour le classement � la fin du 3e tour EUROBOT
  - http://www.coupederobotique.fr/eurobot-2017-ranking-round-n3/
- pour le classement � la fin du 4e tour EUROBOT
  - http://www.coupederobotique.fr/eurobot-2017-ranking-round-n4/
- pour le classement � la fin du 5e tour EUROBOT
  - http://www.coupederobotique.fr/eurobot-2017-ranking-round-n5/

- pour les classements de la coupe de belgique
  - http://robots.pass.be/actualites/coupe-de-belgique-2017-les-resultats.htm?lng=fr

